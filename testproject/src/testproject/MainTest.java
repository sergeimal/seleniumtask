package testproject;

import static org.junit.Assert.assertTrue;
import testproject.Locators;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class MainTest {
	
	private WebDriver driver;
	private boolean result = true;

	@Before
	public void setUp() throws Exception {
		driver = new FirefoxDriver();
	}

	@Test
	public void testTable() throws InterruptedException {
		driver.get("http://localhost:9090/knowit/");
		// Verify header
		String header = driver.findElement(By.xpath(Locators.PAGE_HEADER))
				.getText();
		assertTrue("Page header incorrect or does not exists",
				header.contains("Knowit university achievements list"));
		Thread.sleep(1000);

		// Click on Sorting by id
		driver.findElement(By.xpath(Locators.COLUMN_ONE)).click();
		Thread.sleep(1000);

		// CalculateAverage
		assertTrue("Grades testing failed", CalculateAverage());

		// Click on Sorting by average
		driver.findElement(By.xpath(Locators.COLUMN_SIX)).click();
		Thread.sleep(1000);

		// TestSorting
		assertTrue("Sorting testing failed", SortingTest());
		

	}


	public boolean CalculateAverage() {
		try {
			FileInputStream file = new FileInputStream(new File("C:/data.xml"));

			DocumentBuilderFactory builderFactory = DocumentBuilderFactory
					.newInstance();

			DocumentBuilder builder = builderFactory.newDocumentBuilder();

			Document xmlDocument = builder.parse(file);

			XPath xPath = XPathFactory.newInstance().newXPath();
			// Go throw all the rows in table
			int RowCount = driver.findElements(By.xpath(Locators.ALL_ROWS))
					.size();
			for (int i = 1; i <= RowCount; i++) {

				// Parse from xml weight
				String weightonexpath = "/students/student[@id='" + i * 100
						+ "']/grades/grade[1]/weight";
				String weightone = xPath.compile(weightonexpath).evaluate(
						xmlDocument);
				double dblweightone = Double.parseDouble(weightone);
				System.out.println(dblweightone);

				String weightwoxpath = "/students/student[@id='" + i * 100
						+ "']/grades/grade[2]/weight";
				String weighttwo = xPath.compile(weightwoxpath).evaluate(
						xmlDocument);
				double dblweighttwo = Double.parseDouble(weighttwo);
				System.out.println(dblweighttwo);

				String weightthreexpath = "/students/student[@id='" + i * 100
						+ "']/grades/grade[3]/weight";
				String weightthree = xPath.compile(weightthreexpath).evaluate(
						xmlDocument);
				double dblweightthree = Double.parseDouble(weightthree);
				System.out.println(dblweightthree);

				// Parse from xml values
				String valueonexpath = "/students/student[@id='" + i * 100
						+ "']/grades/grade[1]/value";
				String valueone = xPath.compile(valueonexpath).evaluate(
						xmlDocument);
				double dblvalueone = Double.parseDouble(valueone);
				System.out.println(dblvalueone);

				String valuewoxpath = "/students/student[@id='" + i * 100
						+ "']/grades/grade[2]/value";
				String valuetwo = xPath.compile(valuewoxpath).evaluate(
						xmlDocument);
				double dblvaluetwo = Double.parseDouble(valuetwo);
				System.out.println(dblvaluetwo);

				String valuethreexpath = "/students/student[@id='" + i * 100
						+ "']/grades/grade[3]/value";
				String valuethree = xPath.compile(valuethreexpath).evaluate(
						xmlDocument);
				double dblvaluethree = Double.parseDouble(valuethree);
				System.out.println(dblvaluethree);

				// Math of Actual value
				double ActualGrade = Math
						.ceil(((((dblvalueone * dblweightone)
								+ (dblvaluetwo * dblweighttwo) + (dblvaluethree * dblweightthree)) / (dblweightone
								+ dblweighttwo + dblweightthree))) * 100) / 100;
				System.out.println("Actual grade of id " + i * 100 + " is: "
						+ ActualGrade);

				// Compare actual to with value in web

				String WebGrade = driver.findElement(
						By.xpath("/html/body/div[1]/div/table/tbody[2]/tr[" + i
								+ "]/td[6]")).getText();
				System.out.println("Web grade of id " + i * 100 + " is: "
						+ WebGrade);
				WebGrade = WebGrade.replaceAll(",", ".");
				double dblwebgrade = Double.parseDouble(WebGrade);
				
				if (ActualGrade != dblwebgrade) {
					System.out.println("Grade mismatch" + "\n\n");
					result = false;
				}
				

			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		
		return result;

	}

	public boolean SortingTest() {
		int RowCount = driver.findElements(By.xpath(Locators.ALL_ROWS)).size();

		System.out.println("\nRowsSize: " + RowCount + "\n");

		for (int i = 1; i < RowCount; i++) {
			String RowTextFirst = driver.findElement(
					By.xpath("/html/body/div[1]/div/table/tbody[2]/tr[" + i
							+ "]/td[6]")).getText();
			int b = i + 1;
			String RowTextSecond = driver.findElement(
					By.xpath("/html/body/div[1]/div/table/tbody[2]/tr[" + b
							+ "]/td[6]")).getText();
			RowTextFirst = RowTextFirst.replaceAll(",", ".");
			RowTextSecond = RowTextSecond.replaceAll(",", ".");
			System.out.println("\n" + RowTextFirst);
			System.out.println(RowTextSecond);
			double first = Double.parseDouble(RowTextFirst);
			double second = Double.parseDouble(RowTextSecond);
			boolean result = true;
			if (first > second) {
				System.out.println("Wrong sorting" + "\n\n");
				result = false;
			}
			assertTrue("Sorting testing failed", result);
		}
		return result;

	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
